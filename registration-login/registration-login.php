<?php 
/**
 * Plugin Name: Register And Login System
 * Author: Nayon
 * Author URI: http://nayonbd.com
 * Description: Register And Login System
 * Version: 1.0
*/

// Exit if accessed directly
if (!defined('ABSPATH')) {
    exit;
}

class Awig_main_class{

	public function __construct(){

		add_action('init',array($this,'Awig_main_area'));
		add_action('wp_enqueue_scripts',array($this,'Awig_main_script_area'));
		add_action('widgets_init',array($this,'Awpg_gallery_widget_area'));
	}


	public function Awig_main_area(){
	
		global $wpdb;
		session_start();
		$prefix=$wpdb->prefix;

		$table = $prefix.'register';
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta("CREATE TABLE $table (
			id int auto_increment,
			name varchar(250),
			email text,
			username text,
			password text,
			unique key id (id))");

		if(isset($_POST['submit']) && $_POST['submit']=='Register'){
			
			$prefix=$wpdb->prefix;
			$table = $prefix.'register';
			global $wpdb;

			$name= $_POST['name'];
			$email= $_POST['email'];
			$username= $_POST['username'];
			$password= $_POST['password'];
			$results = array();
			$results =  $wpdb->get_row("select * FROM $table WHERE email = '".$_POST['email']."'",ARRAY_A);
			if(empty($results)){
				$wpdb->insert($table,array(
				'name'=>$name,
				'email'=>$email,
				'username'=>$username,
				'password'=>$password,
			));
				$_SESSION['login_successfully_message'] = 'your register have been successfully';
			}
			else{
				$_SESSION['existing_email_message'] = 'This email is already exist';
			}
			wp_redirect( home_url() );
			exit;

			}
			if(isset($_POST['submit']) && $_POST['submit']=='Login'){
				$prefix=$wpdb->prefix;
				$table = $prefix.'register';
				$results =  $wpdb->get_row("select * FROM $table WHERE email = '".$_POST['email']."' && password = '".$_POST['password']."'",ARRAY_A);
				if(!empty($results)){
					$_SESSION['login']=1;
					$_SESSION['login_array']=array(
						'id'=>$results['id']
						);
				}
				else{
					$_SESSION['login_error_message'] = 'password or email does not match';
					$_SESSION['tab']='Login';

					wp_redirect( home_url() );
					exit();

				}
				wp_redirect( home_url() );
				exit;
			}
			if(isset($_POST['submit']) && $_POST['submit']=='logout'){
				session_destroy();
				wp_redirect( home_url() );
				exit;
			}

		load_plugin_textdomain('Awig_photo_textdomain', false, dirname( __FILE__).'/lang');


		
	}
	public function Awig_main_script_area(){

		wp_enqueue_style('bootstrapcss',PLUGINS_URL('css/bootstrap.min.css',__FILE__));
		wp_enqueue_style('fontawesomecss',PLUGINS_URL('css/font-awesome.css',__FILE__));
		wp_enqueue_style('jqueryuicss',PLUGINS_URL('css/jquery-ui.min.css',__FILE__));
		wp_enqueue_style('maincss',PLUGINS_URL('css/main.css',__FILE__));

		wp_enqueue_script('bootstrapjs',PLUGINS_URL('js/bootstrap.min.js',__FILE__),array('jquery'));
		wp_enqueue_script('jqueryuijss',PLUGINS_URL('js/jquery-ui.min.js',__FILE__),array('jquery'));
		wp_enqueue_script('customjs',PLUGINS_URL('js/custom.js',__FILE__),array('jquery'));
	}

	public function Awpg_gallery_widget_area(){
		register_widget('Awpg_gallery_photo');
	}


}
new Awig_main_class();

class Awpg_gallery_photo extends wp_widget{
	 /**
	     * Class constructor
	     *
	     * @access public
	     * @return void
	*/
	public function __construct(){
		parent::__construct('register-login','Register And Login',array(
			'description'=>'An easy to use lightbox clone for WordPress.'
		));
	}
		/*
			* widget function of the widget
		*/
	public function widget($args,$instance){
		?>
			<!-- started widget area -->
			<?php echo $args['before_widget'];?>
            <?php
            	if(isset($_SESSION['login']) && !empty($_SESSION['login_array'])){
            		?>
            		<h3>Successfylly loged in</h3>
				<form action=""method="post">
					<button type="submit"value="logout" name="submit" class="btn btn-success">Logout</button>

				</form>
            		<?php
            	}else{

            ?>
				<div class="main-login main-center">
					<ul>
						<li><a href="#register">Register</a></li>
						<li><a href="#login"id="login_btn">Login</a></li>
					</ul>
					<div id="register">
					<form class="form-horizontal" method="post" action="">
						<p class="color-area-sussess"><?php
						if(isset($_SESSION['login_successfully_message'])){
							echo $_SESSION['login_successfully_message'];
							unset($_SESSION['login_successfully_message']);
							
						}
						?></p>
						<div class="form-group">
							<label for="name" class="cols-sm-2 control-label">Your Name</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="name" id="name"  placeholder="Enter your Name" required/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Your Email</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
									<input type="email" class="form-control" name="email" id="email"  placeholder="Enter your Email" required/>
								</div>
							</div>
						</div>
						<p class="color-area"><?php
						if(isset($_SESSION['existing_email_message'])){
							echo $_SESSION['existing_email_message'];
							unset($_SESSION['existing_email_message']);
						}
						?></p>

						<div class="form-group">
							<label for="username" class="cols-sm-2 control-label">Username</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="username" id="username"  placeholder="Enter your Username" required/>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Password</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="password" id="password"  placeholder="Enter your Password" required/>
								</div>
							</div>
						</div>
						<div class="form-group ">
							<input name="submit" type="submit" value="Register" class="btn btn-primary btn-lg btn-block login-button">
						</div>
					</form>
					</div>
					<div id="login">
						<form class="form-horizontal" method="post" action="">
						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Your Email</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
									<input type="email" class="form-control" name="email" id="email"  placeholder="Enter your Email" required/>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Password</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="password" id="password"  placeholder="Enter your Password" required/>
								</div>
							</div>
						</div>
						<div class="form-group ">
							<input name="submit" type="submit" value="Login" class="btn btn-primary btn-lg btn-block login-button">
						</div>
						<?php 
						if(isset($_SESSION['tab']) && $_SESSION['tab']=='Login'){
					
						?>
					<script>
					(function($){
						$(document).ready(function(){
							$( "#login_btn" ).trigger( "click" );
						});
						})(jQuery);				
					</script>
					<?php
					}
					?>
						<p class="color-area"><?php
						if(isset($_SESSION['login_error_message'])){
							echo $_SESSION['login_error_message'];
							unset($_SESSION['login_error_message']);
							unset($_SESSION['tab']);
						}
						?></p>
					</form>
				</div>
			</div>
		<?php }?>

			<?php echo $args['after_widget'];?>
		<!-- ended widget area -->
		<?php
	}
		/*
			* form function of the widget
		*/
	public function form($instance){
	?>

	<?php
	}
}
